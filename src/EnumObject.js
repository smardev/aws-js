/**
 * @file EnumObject class
 * @author Pedro Gomes
 */


/**
 * Provides support for enums
 * @classdesc
 */
exports.EnumObject = class EnumObject {
  /**
   * Creates a new EnumObject using given values
   * @constructor
   *
   * @param {Object} values Enum values
   */
  constructor(values) {
    if (!values) {
      throw new Error('Argument is null or empty (argument name: values)');
    }

    this.valuesCount = 0;
    for (const key in values) {
      if (Object.prototype.hasOwnProperty.call(values, key)) {
        this[key] = values[key];
        this.valuesCount++;
      }
    }
    return Object.freeze(this);
  }

  /**
   * Returns true if the enum has a field with the given name, otherwise false.
   *
   * @param {String} key the value to check for
   * @return {Boolean} True if the enum has a field with the given name, otherwise false.
   */
  isDefined(key) {
    return this.hasOwnProperty(key);
  }
};
