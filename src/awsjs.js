/**
 * @file
 * Entry point for AWSjs library
 *
 */

exports.Connection = require('./lib/Connection').Connection;
exports.EndPoint = require('./lib/EndPoint').EndPoint;
exports.IPPermission = require('./lib/IPPermission').IPPermission;
exports.RDSInstance = require('./lib/RDSInstance').RDSInstance;
exports.RDSInstanceStatus = require('./lib/RDSInstanceStatus').RDSInstanceStatus;
exports.SecurityGroup = require('./lib/SecurityGroup').SecurityGroup;
