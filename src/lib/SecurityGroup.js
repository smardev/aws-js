/**
 * @file SecurityGroup class
 * @author Pedro Gomes
 */

const AWS = require('aws-sdk');
const EC2 = new AWS.EC2();

/**
 * Provides functions to manage AWS EC2 security groups
 * @classdesc
 */
exports.SecurityGroup = class SecurityGroup {
  /**
   * Creates a new SecurityGroup using the given id and name.
   * @constructor
   *
   * @param {string} id The AWS RDS instance id.
   * @param {string} name The AWS RDS instance name.
   */
  constructor(id, name) {
    if (!id) {
      throw new Error('Argument ir null or empty (argument name: id)');
    }

    if (!id) {
      throw new Error('Argument ir null or empty (argument name: id)');
    }

    this.id = id;
    this.arn = arn;
  }

  /**
   * Gets the security group having a given name. If more than one group exist this metho will throw an error.
   *
   * @param {string} name The name of the security group to return
   */
  static async getByName(name) {
    console.log('SecurityGroup::getByName [name: %s]', name);
    const response = await getSecurityGroups([name]);

    if (response.SecurityGroups.length > 1) {
      throw new Error('Query returned more than one security group');
    }

    const result = null;
    if (list.SecurityGroups.length > 0) {
      result = createFromDescription(list.SecurityGroups[0]);
    }
    return result;
  }
};

const getSecurityGroups = async () => {
  EC2.describeSecurityGroups();
};

const createFromDescription = (description) => {
  const result = new SecurityGroup(desc.GroupId, description.GroupName);
  result.Description = description.Description;
};
