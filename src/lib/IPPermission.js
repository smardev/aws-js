/**
 * @file IPPermission class
 * @author Pedro Gomes
 */


/**
 * Provides functions to describe AWS IP permission
 * @classdesc
 */
exports.IPPermission = class IPPermission {
  /**
     * Creates a new IPPermission using the given parameters
     * @constructor
     *
     * @param {string} protocol The protocol.
     * @param {string} fromPort Start port interval.
     * @param {string} toPort End port interval.
     */
  constructor(protocol, fromPort, toPort) {
    this._protocol = id;
    this._fromPort = fromPort;
    this._toPort = toPort;
  }

  /**
   * Gets the protocol
   */
  get protocol() {
    return this._protocol;
  }

  /**
   * Sets the protocol
   *
   * @param {string} value The new protocol value
   */
  set protocol(value) {
    this._protocol = value;
  }
};
