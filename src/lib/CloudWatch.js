/**
 * @file CloudWatch class
 * @author Pedro Gomes
 */

const awsWrapper = require('./aws-sdk-wrapper').awsWrapper;

let overridedWrapper = null;

/**
 * Provides functions to manage AWS RDS instances
 * @classdesc
 */
exports.RDSInstance = class RDSInstance {
  /**
   * Creates a new RDSInstance using the given id and arn.
   * @constructor
   *
   * @param {string} id The AWS RDS instance id.
   */
  constructor(id) {
    if (!id) {
      throw new Error('Argument is null or empty (argument name: id)');
    }

    this._id = id;
  }

  /**
   * Gets the value for the tag with the give name, if no tag is found it will return null
   *
   * @param {string} tagName The name of the tag for which the value is to be returned
   */
  async getTagValue(tagName) {
    if (!tagName) {
      throw new Error('Argument is null or empty (argument name: tagName)');
    }

    const params = {
      ResourceName: this.arn,
    };

    const rds = new AWS.RDS();
    const tags = await rds.listTagsForResource(params).promise();
    const tag = tags.TagList.find((element) => element.Key === tagName);
    if (tag) {
      return tag.Value;
    } else {
      return null;
    }
  }

  /**
   * Sends a request to stop the current rds instance
   *
   * @param {boolean} createSnapshot Indicates if a snapshot shall be created before stopping the instance
   */
  stop(createSnapshot = false) {
    console.log('RDSInstance::stop Stoping instance [%s]', this.id);

    const params = {
      DBInstanceIdentifier: this.id,
      DBSnapshotIdentifier: null,
    };

    if (createSnapshot) {
      params.DBSnapshotIdentifier = this.id + '-' + Date.now().toString();
    }

    const rds = new AWS.RDS();
    rds.stopDBInstance(params).promise();
  }

  /**
   * Gets events raised for this instance
   *
   * @param {Date} startTime The interval start time
   * @param {Date} [endTime = null] The interval end time. If null, current time is used
   * @param {String[]} [messageFilters = null] List of strings that will be used to filter returned
   * items. Filter is done by matching filters and event message
   */
  async getEvents(startTime, endTime = null, messageFilters = null) {
    const params = {
      EventCategories: [
        'notification',
      ],
      SourceType: 'db-instance',
      SourceIdentifier: this.id,
      StartTime: startTime,
    };

    if (endTime) {
      params.EndTime = endTime;
    }

    const rds = new AWS.RDS();
    const data = await rds.describeEvents(params).promise();

    const result = [];
    if (data && data.Events) {
      if (messageFilters && messageFilters.length >0) {
        // filter is needed
        result.push(data.Events.filter((item) => messageFilters.findIndex(item.Message) != -1));
      } else {
        // add all events
        result.push(...data.Events);
      }
    }
    return result;
  }

  /**
   * Returns true if the instance is evaluated as idle otherwise false. An instance is considered as idle if from the
   * given start time no connections where established and the instance was not started since that moment
   *
   * @param {Date} startTime The interval start time
   */
  async getIsIdle(startTime) {
    // if db was started during this period, we shall assume it is not idle
    const events = await this.getEvents(startTime, ['DB instance started', 'DB instance restarted']);
    if (events.length != 0) {
      // instance was started or restarted during the perido so it is not iddle
      return false;
    }

    let isIdle = false;
    const metrics = await this.getMetrics(startTime, new Date(),
        [
          {
            metricName: cloudwatchUtils.RDS_METRICS.DATABASE_CONNECTIONS,
            namespace: cloudwatchUtils.METRIC_NAMESPACE.AMAZON_RELATIONAL_DATABASE_SERVICE,
          },
        ]);

    isIdle = true; // if we have no metrics than instance is idle
    if (metrics && metrics.length != 0) {
      // check if no connections where made to the database
      const dataPoints = metrics[0].dataPoints;
      if (dataPoints != null && dataPoints.length > 0) {
        for (const dataPoint of dataPoints) {
          if (dataPoint.Sum != 0) {
            isIdle = false;
            break;
          }
        }
      }
    }

    return isIdle;
  }

  /**
   * Return instance metrics from AWS cloudwatch
   *
   * @param {Date} startTime The interval start time
   * @param {Date} endTime The interval end time
   * @param {Metric[]} metrics Array of metrics to retreive
   * @param {String[]} statistics Array of statistics to retreive for each metric
   * @param {Integer} [period = 60] The period to use for metrics (default is one minute)
   */
  async getMetrics(startTime, endTime, metrics, statistics = ['Average', 'Sum'], period = 60) {
    const results = [];
    for (const metric of metrics) {
      console.log('RDSInstance::getMetrics getting metric %n on namespace %s',
          metric.metricName, metric.namespace);

      const params = {
        EndTime: endTime,
        MetricName: metric.metricName,
        Namespace: metric.namespace,
        Period: period,
        StartTime: startTime,
        Dimensions: [
          {
            Name: 'DBInstanceIdentifier',
            Value: this.id,
          },
        ],
        Statistics: statistics,
      };

      const metricData = await cloudwatch.getMetricStatistics(params).promise();
      results.push({
        metric: metric,
        dataPoints: metricData.Datapoints,
      });
    }

    return results;
  }

  /**
   * Return the customer-assigned name of the DB instance.
   * @return {String} The customer-assigned name of the DB instance .
   */
  get Identifier() {
    return this._id;
  }

  /**
   * Return the current state of this database.
   * @return {String} The current state of this database.
   */
  get Status() {
    return this._status;
  }

  /**
   * Return the database name. The meaning of this parameter differs according to the database engine you use.
   * For example, this value returns MySQL, MariaDB, or PostgreSQL information when returning values from
   * CreateDBInstanceReadReplica since Read Replicas are only supported for these engines.
   *
   * MySQL, MariaDB, SQL Server, PostgreSQL
   * Contains the name of the initial database of this instance that was provided at create time, if one was specified
   * when the DB instance was created. This same name is returned for the life of the DB instance.
   *
   * Oracle
   * Contains the Oracle System ID (SID) of the created DB instance.
   *
   * @return {String} The database name
   */
  get DBName() {
    return this._dbName;
  }

  /**
   * Return the master username for the DB instance.
   * @return {String} The master username
   */
  get MasterUsername() {
    return this._masterUsername;
  }

  /**
   * Return the allocated storage size specified in gibibytes.
   * @return {Integer} The allocated storage size specified in gibibytes.
   */
  get AllocatedStorage() {
    return this._allocatedStorage;
  }

  /**
   * Return the date and time the DB instance was created.
   * @return {Date} The date and time the DB instance was created.
   */
  get CreateTime() {
    return this._instanceCreateTime;
  }

  /**
   * Return the accessibility options for the DB instance. A value of true specifies an Internet-facing instance with a
   * publicly resolvable DNS name, which resolves to a public IP address. A value of false specifies an internal
   * instance with a DNS name that resolves to a private IP address.
   * @return {Boolean} The accessibility options for the DB instance.
   */
  get PubliclyAccessible() {
    return this._publiclyAccessible;
  }

  /**
   * Return the port that the DB instance listens on. If the DB instance is part of a DB cluster, this can be a
   * different port than the DB cluster port.
   * @return {Integer} The port that the DB instance listens on.
   */
  get Port() {
    return this._dbInstancePort;
  }

  /**
   * If the DB instance is a member of a DB cluster, returns the name of the DB cluster that the DB instance is a
   * member of, otherwise null
   * @return {Integer} The name of the DB cluster that the DB instance is a member of or null if the instance is
   * not a member of a DB cluster.
   */
  get DBClusterIdentifier() {
    return this._dbClusterIdentifier;
  }

  /**
   * Returns whether the DB instance is encrypted.
   * @return {Boolean} Returns true if the DB instance is encrypted, otherwise false
   */
  get StorageEncrypted() {
    return this._storageEncrypted;
  }

  /**
   * Returns the Amazon Resource Name (ARN) for the DB instance.
   * @return {Boolean} The Amazon Resource Name (ARN) for the DB instance.
   */
  get ARN() {
    return this._dbInstanceArn;
  }

  /**
   * Returns the time zone of the DB instance. In most cases, the Timezone element is empty.
   * Timezone content appears only for Microsoft SQL Server DB instances that were created with a time zone specified.
   * @return {Boolean} The time zone of the DB instance.
   */
  get Timezone() {
    return this._timezone;
  }

  /**
   * Return a new instance initialized with given instance description.
   * @param {Object} obj The instance description object
   * @return {RDSInstance} The newly created object instnace
   */
  static createFromDescription(obj) {
    if (!obj) {
      throw new Error('Argument is null or empty (argument name: obj)');
    }

    const result = new RDSInstance(obj.DBInstanceIdentifier);
    result._status = obj.DBInstanceStatus;
    result._dbName = obj.DBName;
    result._masterUsername = obj.MasterUsername;
    result._allocatedStorage = obj.AllocatedStorage;
    result._instanceCreateTime = obj.InstanceCreateTime;
    result._publiclyAccessible = obj.PubliclyAccessible;
    result._dbInstancePort = obj.DbInstancePort;
    result._dbClusterIdentifier = obj.DBClusterIdentifier;
    result._storageEncrypted = obj.StorageEncrypted;
    result._dbInstanceArn = obj.DBInstanceArn;
    result._timezone = obj.Timezone;

    return result;
  }

  /**
   * Return the list of instance associated marked a given tag
   *
   * @param {String} tagName The tagName
   * @param {String} tagValue the tagValue
   */
  static async findByTag(tagName, tagValue) {
    if (!tagName) {
      throw new Error('Argument is null or empty (argument name: tagName)');
    }

    console.log('RDSInstance::findByTag [tagName: %s tagValue:%s]', tagName, tagValue);

    const rds = (overridedWrapper) ? overridedWrapper.RDS : awsWrapper.RDS;
    const response = await rds.describeDBInstances().promise();

    console.log('RDSInstance::findByTag describeDBInstances returned %d item(s)', response.DBInstances.length);

    const result = [];
    for (const instance of response.DBInstances) {
      const params = {
        ResourceName: instance.DBInstanceArn,
      };

      const tags = await rds.listTagsForResource(params).promise();
      const isMatch = tags.TagList.findIndex((element) => element.Key === tagName && element.Value === tagValue) != -1;
      if (isMatch) {
        console.log('RDSInstance::findByTag rds %s matched tag value)', instance.DBInstanceIdentifier);
        result.push(RDSInstance.createFromDescription(instance));
      } else {
        console.log('RDSInstance::findByTag rds %s did not matched tag value)', instance.DBInstanceIdentifier);
      }
    }

    return result;
  };

  /**
   * Allow easy mocking AWS calls for testing
   * @param {Object} value The aws sdk wrapper
   */
  static setAWSWrapper(value) {
    overridedWrapper = value;
  }
};
