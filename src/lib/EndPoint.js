/**
 * @file EndPoint class
 * @author Pedro Gomes
 */


/**
 * Provides functions to describe an end point
 * @classdesc
 */
exports.EndPoint = class EndPoint {
  /**
     * Creates a new EndPoint using the given parameters
     * @constructor
     *
     * @param {String} address The DNS address of the DB instance
     * @param {Integer} port The port that the database engine is listening on.
     * @param {String} hostedZoneId The ID that Amazon Route 53 assigns when you create a hosted zone.
     */
  constructor(address, port, hostedZoneId) {
    if (!address) {
      throw new Error('Argument is null or empty (argument name: address)');
    }

    if (!port) {
      throw new Error('Argument is null or empty (argument name: port)');
    }

    if (!hostedZoneId) {
      throw new Error('Argument is null or empty (argument name: hostedZoneId)');
    }

    this._address = address;
    this._port = port;
    this._hostedZoneId = hostedZoneId;
  }

  /**
   * Gets the DNS address of the DB instance
   */
  get Address() {
    return this._address;
  }

  /**
   * Gets the port that the database engine is listening on.
   */
  get Port() {
    return this._port;
  }

  /**
   * Gets the ID that Amazon Route 53 assigns when you create a hosted zone.
   */
  get HostedZoneId() {
    return this._hostedZoneId;
  }
};
