/**
 * @file Connection class
 * @author Pedro Gomes
 */

const awssdk = require('aws-sdk');

/**
 * Provides functions to manage AWS connection (using a given region, accessKeyId and secretAccessKey)
 * @classdesc
 */
exports.Connection = class Connection {
  /**
   * Creates a new Connection instance
   * @constructor
   *
   * @param {string} region The AWS RDS instance id.
   * @param {string} accessKeyId The AWS RDS instance id.
   * @param {string} secretAccessKey The AWS RDS instance id.
   */
  constructor(region, accessKeyId = null, secretAccessKey = null) {
    if (!region) {
      throw new Error('Argument is null or empty (argument name: region)');
    }

    this._region = region;

    if (!accessKeyId && !secretAccessKey) {
      this._credentials = null;
    } else if (accessKeyId && secretAccessKey) {
      this._credentials = {accessKeyId: accessKeyId, secretAccessKey: secretAccessKey};
    } else {
      throw new Error('Invalid arguments. Pass both accessKeyId and secretAccessKey or none to use environment');
    }
  }

  /**
   * Return the connection region.
   * @return {String} The region name.
   */
  get Region() {
    return this._region;
  }

  /**
   * Opens the connection to AWS
  * @return {AWS.Credentials} The connection credentials.
   */
  getCredentials() {
    if (this._credentials) {
      return new awssdk.Credentials(this._credentials);
    } else {
      return new awssdk.SharedIniFileCredentials();
    }
  }
};
