/**
 * @file RDSInstance class
 * @author Pedro Gomes
 */

const RDSService = require('./RDSService').RDSService;

/**
 * Provides functions to manage AWS RDS instances
 * @classdesc
 */
const RDSInstance = class RDSInstance {
  /**
   * Creates a new RDSInstance using the given id and arn.
   * @constructor
   *
   * @param {string} id The AWS RDS instance id.
   */
  constructor(id) {
    if (!id) {
      throw new Error('Argument is null or empty (argument name: id)');
    }

    this._connection = null;
    this._id = id;
  }

  /**
   * Binds this instance to a given connection
   *
   * @param {Connection} connection The connection instnace
   */
  bind(connection) {
    if (!connection) {
      throw new Error('Argument is null or empty (argument name: connection)');
    }

    if (this._connection) {
      throw new Error('Instance already binded, rebind not supported');
    }

    this._connection = connection;
  }

  /**
   * Gets the value for the tag with the give name, if no tag is found it will return null
   *
   * @param {string} tagName The name of the tag for which the value is to be returned
   */
  async getTagValue(tagName) {
    if (!tagName) {
      throw new Error('Argument is null or empty (argument name: tagName)');
    }

    const params = {
      ResourceName: this.ARN,
    };

    const tags = await this._getService().listTagsForResource(params).promise();
    const tag = tags.TagList.find((element) => {
      return element.Key === tagName;
    });
    return tag ? tag.Value : null;
  }

  /**
   * Returns the RDSService instance
   *
   * @return {RDSService} The rds service
   */
  _getService() {
    if (!this._connection) {
      throw new Error('Instance not binded to a connection');
    }

    if (!this._rdsService) {
      this._rdsService = new RDSService(this._connection);
    }

    return this._rdsService;
  }

  /**
   * Sends a request to stop the current rds instance
   *
   * @param {boolean} createSnapshot Indicates if a snapshot shall be created before stopping the instance
   * @return {promise} A  returns a 'thenable' promise. Two callbacks can be provided to the then method on the
   * returned promise. The first callback will be called if the promise is fulfilled, and the second callback will be
   * called if the promise is rejected.
   */
  stop(createSnapshot = false) {
    console.log('RDSInstance::stop Stoping instance [%s]', this.id);

    const params = {
      DBInstanceIdentifier: this._id,
      DBSnapshotIdentifier: null,
    };

    if (createSnapshot) {
      const date = new Date();
      const timestamp = date.getTime();
      params.DBSnapshotIdentifier = this.id + '-' + timestamp;
    }

    return this._getService().stopDBInstance(params).promise();
  }

  /**
   * Sends a request to start the current rds instance
   *
   * @return {promise} A  returns a 'thenable' promise. Two callbacks can be provided to the then method on the
   * returned promise. The first callback will be called if the promise is fulfilled, and the second callback will be
   * called if the promise is rejected.
   */
  start() {
    console.log('RDSInstance::start Starting instance [%s]', this.id);

    if (!this._connection) {
      throw new Error('Instance not binded to a connection');
    }

    const params = {
      DBInstanceIdentifier: this._id,
    };

    return this._getService().startDBInstance(params).promise();
  }

  /**
   * Refreshs the metadata information for the current instance
   */
  async refresh() {
    console.log('RDSInstance::refresh Getting information for instance [%s]', this.id);

    if (!this._connection) {
      throw new Error('Instance not binded to a connection');
    }

    const params = {
      DBInstanceIdentifier: this._id,
    };

    const response = await this._getService().describeDBInstances(params).promise();
    if (response.DBInstances.length == 0) {
      console.log('RDSInstance::refresh describeDBInstances returned %d item(s)', response.DBInstances.length);
      throw new Error('DB Instance information not retreived from AWS, please check object id');
    }

    _initializeFromDescription(this, response.DBInstances[0]);
  };

  /**
   * Gets events raised for this instance
   *
   * @param {Date} startTime The interval start time
   * @param {Date} [endTime = null] The interval end time. If null, current time is used
   * @param {String[]} [messageFilters = null] List of strings that will be used to filter returned
   * items. Filter is done by matching filters and event message
   */
  async getEvents(startTime, endTime = null, messageFilters = null) {
    const params = {
      EventCategories: [
        'notification',
      ],
      SourceType: 'db-instance',
      SourceIdentifier: this.id,
      StartTime: startTime,
    };

    if (endTime) {
      params.EndTime = endTime;
    }

    const rds = new AWS.RDS();
    const data = await rds.describeEvents(params).promise();

    const result = [];
    if (data && data.Events) {
      if (messageFilters && messageFilters.length >0) {
        // filter is needed
        result.push(data.Events.filter((item) => messageFilters.findIndex(item.Message) != -1));
      } else {
        // add all events
        result.push(...data.Events);
      }
    }
    return result;
  }

  /**
   * Returns true if the instance is evaluated as idle otherwise false. An instance is considered as idle if from the
   * given start time no connections where established and the instance was not started since that moment
   *
   * @param {Date} startTime The interval start time
   */
  async getIsIdle(startTime) {
    // if db was started during this period, we shall assume it is not idle
    const events = await this.getEvents(startTime, ['DB instance started', 'DB instance restarted']);
    if (events.length != 0) {
      // instance was started or restarted during the perido so it is not iddle
      return false;
    }

    let isIdle = false;
    const metrics = await this.getMetrics(startTime, new Date(),
        [
          {
            metricName: cloudwatchUtils.RDS_METRICS.DATABASE_CONNECTIONS,
            namespace: cloudwatchUtils.METRIC_NAMESPACE.AMAZON_RELATIONAL_DATABASE_SERVICE,
          },
        ]);

    isIdle = true; // if we have no metrics than instance is idle
    if (metrics && metrics.length != 0) {
      // check if no connections where made to the database
      const dataPoints = metrics[0].dataPoints;
      if (dataPoints != null && dataPoints.length > 0) {
        for (const dataPoint of dataPoints) {
          if (dataPoint.Sum != 0) {
            isIdle = false;
            break;
          }
        }
      }
    }

    return isIdle;
  }

  /**
   * Return instance metrics from AWS cloudwatch
   *
   * @param {Date} startTime The interval start time
   * @param {Date} endTime The interval end time
   * @param {Metric[]} metrics Array of metrics to retreive
   * @param {String[]} statistics Array of statistics to retreive for each metric
   * @param {Integer} [period = 60] The period to use for metrics (default is one minute)
   */
  async getMetrics(startTime, endTime, metrics, statistics = ['Average', 'Sum'], period = 60) {
    const results = [];
    for (const metric of metrics) {
      console.log('RDSInstance::getMetrics getting metric %n on namespace %s',
          metric.metricName, metric.namespace);

      const params = {
        EndTime: endTime,
        MetricName: metric.metricName,
        Namespace: metric.namespace,
        Period: period,
        StartTime: startTime,
        Dimensions: [
          {
            Name: 'DBInstanceIdentifier',
            Value: this.id,
          },
        ],
        Statistics: statistics,
      };

      const metricData = await cloudwatch.getMetricStatistics(params).promise();
      results.push({
        metric: metric,
        dataPoints: metricData.Datapoints,
      });
    }

    return results;
  }

  /**
   * Return the customer-assigned name of the DB instance.
   * @return {String} The customer-assigned name of the DB instance .
   */
  get Identifier() {
    return this._id;
  }

  /**
   * Return the current state of this database.
   * @return {String} The current state of this database.
   */
  get Status() {
    return this._status;
  }

  /**
   * Return the database name. The meaning of this parameter differs according to the database engine you use.
   * For example, this value returns MySQL, MariaDB, or PostgreSQL information when returning values from
   * CreateDBInstanceReadReplica since Read Replicas are only supported for these engines.
   *
   * MySQL, MariaDB, SQL Server, PostgreSQL
   * Contains the name of the initial database of this instance that was provided at create time, if one was specified
   * when the DB instance was created. This same name is returned for the life of the DB instance.
   *
   * Oracle
   * Contains the Oracle System ID (SID) of the created DB instance.
   *
   * @return {String} The database name
   */
  get DBName() {
    return this._dbName;
  }

  /**
   * Return the master username for the DB instance.
   * @return {String} The master username
   */
  get MasterUsername() {
    return this._masterUsername;
  }

  /**
   * Return the allocated storage size specified in gibibytes.
   * @return {Integer} The allocated storage size specified in gibibytes.
   */
  get AllocatedStorage() {
    return this._allocatedStorage;
  }

  /**
   * Return the date and time the DB instance was created.
   * @return {Date} The date and time the DB instance was created.
   */
  get CreateTime() {
    return this._instanceCreateTime;
  }

  /**
   * Return the accessibility options for the DB instance. A value of true specifies an Internet-facing instance with a
   * publicly resolvable DNS name, which resolves to a public IP address. A value of false specifies an internal
   * instance with a DNS name that resolves to a private IP address.
   * @return {Boolean} The accessibility options for the DB instance.
   */
  get PubliclyAccessible() {
    return this._publiclyAccessible;
  }

  /**
   * Return the port that the DB instance listens on. If the DB instance is part of a DB cluster, this can be a
   * different port than the DB cluster port.
   * @return {Integer} The port that the DB instance listens on.
   */
  get Port() {
    return this._dbInstancePort;
  }

  /**
   * If the DB instance is a member of a DB cluster, returns the name of the DB cluster that the DB instance is a
   * member of, otherwise null
   * @return {Integer} The name of the DB cluster that the DB instance is a member of or null if the instance is
   * not a member of a DB cluster.
   */
  get DBClusterIdentifier() {
    return this._dbClusterIdentifier;
  }

  /**
   * Returns whether the DB instance is encrypted.
   * @return {Boolean} Returns true if the DB instance is encrypted, otherwise false
   */
  get StorageEncrypted() {
    return this._storageEncrypted;
  }

  /**
   * Returns the Amazon Resource Name (ARN) for the DB instance.
   * @return {Boolean} The Amazon Resource Name (ARN) for the DB instance.
   */
  get ARN() {
    return this._dbInstanceArn;
  }

  /**
   * Returns the time zone of the DB instance. In most cases, the Timezone element is empty.
   * Timezone content appears only for Microsoft SQL Server DB instances that were created with a time zone specified.
   * @return {Boolean} The time zone of the DB instance.
   */
  get Timezone() {
    return this._timezone;
  }

  /**
   * Return the list of instances
   * @param {Connection} connection The connection instance
   */
  static async getAll(connection) {
    if (!connection) {
      throw new Error('Argument is null or empty (argument name: connection)');
    }

    const response = await _getDBInstances(connection).promise();
    console.log('RDSInstance::getAll describeDBInstances returned %d item(s)', response.DBInstances.length);

    const result = [];
    for (const instance of response.DBInstances) {
      const obj = _createFromDescription(instance);
      obj.bind(connection);
      result.push(obj);
    }

    return result;
  };

  /**
   * Return the list of instance associated marked a given tag
   * @param {Connection} connection The connection instance
   * @param {String} tagName The tagName
   * @param {String} tagValue the tagValue
   */
  static async findByTag(connection, tagName, tagValue) {
    if (!connection) {
      throw new Error('Argument is null or empty (argument name: connection)');
    }

    if (!tagName) {
      throw new Error('Argument is null or empty (argument name: tagName)');
    }

    const rds = new RDSService(connection);
    const response = await _getDBInstances(connection).promise();

    const result = [];
    for (const instance of response.DBInstances) {
      const params = {
        ResourceName: instance.DBInstanceArn,
      };

      const tags = await rds.listTagsForResource(params).promise();
      const isMatch = tags.TagList.findIndex((element) => {
        return element.Key === tagName && element.Value === tagValue;
      }) != -1;

      if (isMatch) {
        const obj = _createFromDescription(instance);
        obj.bind(connection);
        result.push(obj);
      }
    }

    return result;
  };
};


/**
 * Initializes the given instance with given instance description.
 * @param {RDSInstance} instance The instance to be initialized.
 * @param {Object} description The instance description object.
 */
function _initializeFromDescription(instance, description) {
  instance._id = description.DBInstanceIdentifier;
  instance._status = description.DBInstanceStatus;
  instance._dbName = description.DBName;
  instance._masterUsername = description.MasterUsername;
  instance._allocatedStorage = description.AllocatedStorage;
  instance._instanceCreateTime = description.InstanceCreateTime;
  instance._publiclyAccessible = description.PubliclyAccessible;
  instance._dbInstancePort = description.DbInstancePort;
  instance._dbClusterIdentifier = description.DBClusterIdentifier;
  instance._storageEncrypted = description.StorageEncrypted;
  instance._dbInstanceArn = description.DBInstanceArn;
  instance._timezone = description.Timezone;
}

/**
 * Return a new instance initialized with given instance description.
 * @param {Object} obj The instance description object
 * @return {RDSInstance} The newly created object instnace
 */
function _createFromDescription(obj) {
  const result = new RDSInstance(obj.DBInstanceIdentifier);
  _initializeFromDescription(result, obj);
  return result;
}

/**
 * Return the description of all RDS instances.
 * @param {Connection} connection The connection.
 * @return {AWSRequest} The request to the AWS RDS service
 */
function _getDBInstances(connection) {
  const rds = new RDSService(connection);
  return rds.describeDBInstances();
};

exports.RDSInstance = RDSInstance;
