/**
 * @file RDSService class
 * @author Pedro Gomes
 */

const awssdk = require('aws-sdk');
const Connection = require('./Connection').Connection;

/**
 * Wrapper for AWS SDK services for RDS instances.
 * @classdesc
 */
const RDSService = class RDSService {
  /**
   * Creates a new RDSService.
   * @constructor
   *
   * @param {Connection} connection The connection object.
   */
  constructor(connection) {
    if (!connection) {
      throw new Error('Argument is null or empty (argument name: connection)');
    }

    if (!(connection instanceof Connection)) {
      throw new Error('Invalid argument. Not and instance of AWSjs.Connection (argument name: connection)');
    }

    this._credentials = connection.getCredentials();
    this._service = new awssdk.RDS({
      credentials: this._credentials, apiVersion: '2014-10-31', region: connection.Region});
  };

  /**
   * Calls the describeDBInstances service from the RDS service on AWS SDK
   * (https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/RDS.html#describeDBInstances-property)
   * @param {Object} params Please check AWS SDK documentation for more information.
   * @param {Object} callback Please check AWS SDK documentation for more information.
   * @return {Object} Please check AWS SDK documentation for more information.
   */
  describeDBInstances(params, callback) {
    return this._service.describeDBInstances(params, callback);
  }

  /**
   * Calls the listTagsForResource service from the RDS service on AWS SDK
   * (https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/RDS.html#listTagsForResource-property)
   * @param {Object} params Please check AWS SDK documentation for more information.
   * @param {Object} callback Please check AWS SDK documentation for more information.
   * @return {Object} Please check AWS SDK documentation for more information.
   */
  listTagsForResource(params, callback) {
    return this._service.listTagsForResource(params, callback);
  }

  /**
   * Calls the stopDBInstance service from the RDS service on AWS SDK
   * (https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/RDS.html#stopDBInstance-property)
   * @param {Object} params Please check AWS SDK documentation for more information.
   * @param {Object} callback Please check AWS SDK documentation for more information.
   * @return {Object} Please check AWS SDK documentation for more information.
   */
  stopDBInstance(params, callback) {
    return this._service.stopDBInstance(params, callback);
  }

  /**
   * Calls the startDBInstance service from the RDS service on AWS SDK
   * (https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/RDS.html#startDBInstance-property)
   * @param {Object} params Please check AWS SDK documentation for more information.
   * @param {Object} callback Please check AWS SDK documentation for more information.
   * @return {Object} Please check AWS SDK documentation for more information.
   */
  startDBInstance(params, callback) {
    return this._service.stopDBInstance(params, callback);
  }
};

exports.RDSService = RDSService;
