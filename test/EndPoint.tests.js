import {EndPoint} from './../src/awsjs';

describe('EndPoint class unit tests', () => {
  beforeAll(() => {
  });

  afterAll(() => {
  });

  beforeEach(() => {
  });

  afterEach(() => {
  });

  test('Constructor - Invariants Check', () => {
    // Should throw an error with "Argument ir null or empty (argument name: address)" text if id is null
    expect((() => new EndPoint())).toThrowError('Argument is null or empty (argument name: address)');

    // Should throw an error with "Argument ir null or empty (argument name: port)" text if id is null
    expect((() => new EndPoint('address'))).toThrowError('Argument is null or empty (argument name: port)');

    // Should throw an error with "Argument ir null or empty (argument name: hostedZoneId)" text if id is null
    expect((() => new EndPoint('address', 10))).toThrowError('Argument is null or empty (argument name: hostedZoneId)');
  });

  test('Constructor - Instance Creation', () => {
    const obj = new EndPoint('address', 10, 'hosted zone id');

    expect(obj.Address).toBe('address');
    expect(obj.Port).toBe(10);
    expect(obj.HostedZoneId).toBe('hosted zone id');
  });
});
