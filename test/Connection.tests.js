'use strict';

import {Connection} from '../src/awsjs';
import sinon from 'sinon';
const awssdk = require('aws-sdk');

describe('Connection class unit tests', () => {
  beforeAll(() => {
  });

  afterAll(() => {
  });

  beforeEach(() => {
  });

  afterEach(() => {
  });

  test('Constructor - Invariants Check', () => {
    // Should throw an error with "Argument ir null or empty (argument name: region)" text if region is null
    expect((() => new Connection())).toThrowError('Argument is null or empty (argument name: region)');

    // Should throw an error with "Invalid arguments. Pass both accessKeyId and secretAccessKey or none to use
    // environment" if only accessKeyId or only  secretAccessKey is passed
    expect((() => new Connection('region', 'a'))).
        toThrowError('Invalid arguments. Pass both accessKeyId and secretAccessKey or none to use environment');
    expect((() => new Connection('region', null, 'a'))).
        toThrowError('Invalid arguments. Pass both accessKeyId and secretAccessKey or none to use environment');
  });

  test('getCredentials', () => {
    const spy = sinon.spy(awssdk, 'Credentials');
    const connection = new Connection('region', 'accessKey', 'secretKey');
    const credentials = connection.getCredentials();

    expect(credentials).not.toEqual(null);
    expect(spy.calledOnce).toBe(true);

    const args = spy.getCall(0).args[0];
    expect(args.accessKeyId).toBe( 'accessKey');
    expect(args.secretAccessKey).toBe( 'secretKey');
  });
});
