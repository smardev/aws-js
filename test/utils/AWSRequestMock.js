/**
 * @file AWSRequestMock class
 * @author Pedro Gomes
 */


/**
 * Provides support for mocking methods that return and AWS.Request object
 * @classdesc
 */
exports.AWSRequestMock = class AWSRequestMock {
  /**
   * Creates a new AWSRequestMock using given values
   * @constructor
   *
   * @param {Object} result The data or function to call on returning.
   */
  constructor(result) {
    this._result = result;
  }

  /**
   * Returns a resolved promise
   * @return {Promise} Resolved promise
   */
  promise() {
    if (typeof this._result === 'function') {
      return Promise.resolve(this._result());
    } else {
      return Promise.resolve(this._result);
    }
  }
};
