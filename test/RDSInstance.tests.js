'use strict';

import {RDSInstance, Connection} from '../src/awsjs';
import {RDSService} from '../src/lib/RDSService';
import {AWSRequestMock} from './utils/AWSRequestMock';
import sinon from 'sinon';
import rdsInstancesData from './data/dbinstances.json';

describe('RDSInstance class unit tests', () => {
  beforeAll(() => {
  });

  afterAll(() => {
  });

  beforeEach(() => {
  });

  afterEach(() => {
  });

  test('Constructor - Invariants Check', () => {
    // Should throw an error with "Argument ir null or empty (argument name: id)" text if id is null
    expect((() => new RDSInstance())).toThrowError('Argument is null or empty (argument name: id)');
  });

  test('bind to connection', async () => {
    let errorMessage = 'No error thrown.';
    try {
      const obj = new RDSInstance('id');
      await obj.start();
    } catch (error) {
      errorMessage = error.message;
    }

    // Should throw an error with "Instance not binded to a connection" as bind method was not called
    expect(errorMessage).toBe('Instance not binded to a connection');
  });

  test('start', async () => {
    const stub = sinon.stub(RDSService.prototype, 'startDBInstance').
        callsFake((params, callback) => {
          return new AWSRequestMock();
        });

    const obj = new RDSInstance('id');
    obj.bind(new Connection('region'));
    await obj.start();

    expect(stub.calledOnce).toBe(true);

    const arg = stub.getCall(0).args[0];
    expect(arg.DBInstanceIdentifier).toBe('id');

    stub.restore();
  });

  test('stop', async () => {
    const stub = sinon.stub(RDSService.prototype, 'stopDBInstance').
        callsFake((params, callback) => {
          return new AWSRequestMock();
        });

    const obj = new RDSInstance('id');
    obj.bind(new Connection('region'));
    await obj.stop(false);
    let arg = stub.getCall(0).args[0];
    expect(arg.DBInstanceIdentifier).toBe('id');
    expect(arg.DBSnapshotIdentifier).toBeNull();

    await obj.stop(true);
    arg = stub.getCall(1).args[0];
    expect(arg.DBInstanceIdentifier).toBe('id');
    expect(arg.DBSnapshotIdentifier).not.toEqual(null);

    stub.restore();
  });

  test('getTagValue - Invariant Check', async () => {
    let errorMessage = 'No error thrown.';
    try {
      const obj = new RDSInstance('id');
      await obj.getTagValue();
    } catch (error) {
      errorMessage = error.message;
    }

    // Should throw an error with "Argument ir null or empty (argument name: tagName)" text if tagName is null
    expect(errorMessage).toBe('Argument is null or empty (argument name: tagName)');
  });

  test('getTagValue - Get Value', async () => {
    const stub = sinon.stub(RDSService.prototype, 'listTagsForResource');
    stub.onThirdCall().returns(
        new AWSRequestMock({TagList: [{Key: 'name1', Value: 'valueq'}, {Key: 'name', Value: 'value'}]}));
    stub.returns(new AWSRequestMock({TagList: []}));

    const obj = new RDSInstance('id');
    obj.bind(new Connection('region'));

    let value = await obj.getTagValue('name');
    expect(value).toBeNull();

    value = await obj.getTagValue('name');
    expect(value).toBeNull();

    value = await obj.getTagValue('name');
    expect(value).toBe('value');

    expect(stub.callCount).toBe(3);

    stub.restore();
  });

  test('findByTag - Invariant Check', async () => {
    let errorMessage = 'No error thrown.';
    try {
      await RDSInstance.findByTag();
    } catch (error) {
      errorMessage = error.message;
    }

    // Should throw an error with "Argument ir null or empty (argument name: connection)" text if connection is null
    expect(errorMessage).toBe('Argument is null or empty (argument name: connection)');

    try {
      await RDSInstance.findByTag(new Connection('region'));
    } catch (error) {
      errorMessage = error.message;
    }

    // Should throw an error with "Argument ir null or empty (argument name: tagName)" text if tagName is null
    expect(errorMessage).toBe('Argument is null or empty (argument name: tagName)');
  });

  test('findByTag - Invalid connection argument', async () => {
    let errorMessage = 'No error thrown.';
    try {
      await await RDSInstance.findByTag('connection', 'name', 'value');
    } catch (error) {
      errorMessage = error.message;
    }

    // Should throw an error with "Invalid argument. Not and instance of AWSjs.Connection (argument name: connection)"
    // as passed connection argument is not of type AWSjs.Connection
    expect(errorMessage).toBe('Invalid argument. Not and instance of AWSjs.Connection (argument name: connection)');
  });

  test('findByTag - No DB Instances', async () => {
    const stub = sinon.stub(RDSService.prototype, 'describeDBInstances').
        callsFake((params, callback) => {
          return new AWSRequestMock({DBInstances: []});
        });

    const result = await RDSInstance.findByTag(new Connection('region'), 'name', 'value');
    expect(stub.calledOnce).toBe(true);
    expect(result.length).toBe(0); // validate that no instance was returned

    stub.restore();
  });

  test('findByTag', async () => {
    const stub = sinon.stub(RDSService.prototype, 'describeDBInstances').
        callsFake((params, callback) => {
          return new AWSRequestMock({DBInstances: rdsInstancesData});
        });

    const stub2 = sinon.stub(RDSService.prototype, 'listTagsForResource');
    stub2.onSecondCall().returns(
        new AWSRequestMock({TagList: [{Key: 'name', Value: 'value'}, {Key: 'name1', Value: 'value1'}]}));
    stub2.returns(new AWSRequestMock({TagList: []}));

    const result = await RDSInstance.findByTag(new Connection('region'), 'name', 'value');
    expect(stub.calledOnce).toBe(true);
    expect(stub2.callCount).toBe(rdsInstancesData.length);
    expect(result.length).toBe(1); // validate that no instance was returned
    expect(result[0].Identifier).toBe('2.prop1');

    stub.restore();
  });

  test('getAll', async () => {
    const stub = sinon.stub(RDSService.prototype, 'describeDBInstances').
        callsFake((params, callback) => {
          return new AWSRequestMock({DBInstances: rdsInstancesData});
        });

    const result = await RDSInstance.getAll(new Connection('region'), 'name', 'value');
    expect(stub.calledOnce).toBe(true);
    expect(result.length).toBe(rdsInstancesData.length);

    stub.restore();
  });

  test('refresh wrong id', async () => {
    const stub = sinon.stub(RDSService.prototype, 'describeDBInstances').
        callsFake((params, callback) => {
          return new AWSRequestMock({DBInstances: []});
        });

    let errorMessage = 'No error thrown.';
    const obj = new RDSInstance('id');
    obj.bind(new Connection('region'));
    try {
      await obj.refresh();
    } catch (error) {
      errorMessage = error.message;
    }

    // Should throw an error with "DB Instance information not retreived by AWS, please check object id" text
    // instance id does not match one existing on aws
    expect(stub.calledOnce).toBe(true);
    expect(errorMessage).toBe('DB Instance information not retreived from AWS, please check object id');

    stub.restore();
  });

  test('refresh', async () => {
    const stub = sinon.stub(RDSService.prototype, 'describeDBInstances').
        callsFake((params, callback) => {
          return new AWSRequestMock({DBInstances: [rdsInstancesData[0]]});
        });

    const instance = new RDSInstance('id');
    instance.bind(new Connection('Region'));
    await instance.refresh();

    expect(stub.calledOnce).toBe(true);
    expect(instance.Identifier).toBe('1.prop1');
    expect(instance.Status).toBe('1.prop2');
    expect(instance.DBName).toBe('1.prop3');
    expect(instance.MasterUsername).toBe('1.prop4');
    expect(instance.AllocatedStorage).toBe('1.prop5');
    expect(instance.CreateTime).toBe('1.prop6');
    expect(instance.PubliclyAccessible).toBe('1.prop7');
    expect(instance.Port).toBe('1.prop8');
    expect(instance.DBClusterIdentifier).toBe('1.prop9');
    expect(instance.StorageEncrypted).toBe('1.prop10');
    expect(instance.ARN).toBe('1.prop11');
    expect(instance.Timezone).toBe('1.prop12');

    stub.restore();
  });
});
