import {EnumObject} from './../src/core';

describe('EnumObject class unit tests', () => {
  beforeAll(() => {
  });

  afterAll(() => {
  });

  beforeEach(() => {
  });

  afterEach(() => {
  });

  test('Constructor - Invariants Check', () => {
    // Should throw an error with "Argument is null or empty (argument name: values)" text if values is null
    expect((() => new EnumObject())).toThrowError('Argument is null or empty (argument name: values)');
  });

  test('Constructor - Instance Creation', () => {
    const instance = new EnumObject({
      value1: '1',
      value2: '2',
      value3: '3',
      value4: '4',
    });

    expect(instance.valuesCount).toBe(4);
    expect(instance).toHaveProperty('value1');
    expect(instance).toHaveProperty('value2');
    expect(instance).toHaveProperty('value3');
    expect(instance).toHaveProperty('value4');
  });

  test('isDefined', () => {
    const instance = new EnumObject({
      value1: '1',
      value2: '2',
      value3: '3',
      value4: '4',
    });

    expect(instance.valuesCount).toBe(4);
    expect(instance).toHaveProperty('value1');
    expect(instance).toHaveProperty('value2');
    expect(instance).toHaveProperty('value3');
    expect(instance).toHaveProperty('value4');
  });
});
