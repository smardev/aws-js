# AWSjs
[![Version](https://badge.fury.io/js/%40smardev%2Fawsjs.svg)](https://badge.fury.io/js/%40smardev%2Fawsjs.svg) [![Build Status](https://img.shields.io/circleci/token/25a0282c1bd667f533332ba2c5e551a26d9d685b/project/bitbucket/smardev/aws-js/master.svg)](https://circleci.com/bb/smardev/aws-js) [![Coverage Status](https://img.shields.io/codecov/c/bb/smardev/aws-js.svg)](https://codecov.io/bb/smardev/aws-js)

Javascrit ES6 library built on top of AWS SDK. It aims to make it easier to interact with AWS on your projects.

AWS SDK does not provide a simple object model, it rather encapsulates a set of requests that provide information or
allow changing cloud resources, AWSjs aims to create an object model that hides the SDK complexity

**_Note: This is still a work in progress, bellow are the list of supported objects and completion level_**

* Connection  [DONE]
* EndPoint  [DONE]
* IPPermission [5%]
* RDSInstance [70%]
* RDSInstanceStatus [DONE]
* SecurityGroup [10%]

## Installing

### Pre-requisites
AWS SDK is referenced as development dependency, giving you the capability of not having it as part of your package
when AWS SDK is part of your node runtime (for instance it allows you to minimize your AWS lambda package)

Before installing AWSjs, you need to install and configure AWS SDK, for more information on how to do it, please check
https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/getting-started.html

### In Node.js
The preferred way to install the AWSjs for Node.js is to use the [npm](http://npmjs.org) package manager for Node.js.
Simply type the following into a terminal window:

```sh
npm install @smardev/awsjs
```

Then within your application, you can reference awsjs by

```javascript
var AWSJS = require('awsjs');
```

## Usage and Getting Started

You can find a getting started guide at:

TO BE ADDED

## Usage Samples

The repository contains a samples folder where you can find some AWSjs usage examples