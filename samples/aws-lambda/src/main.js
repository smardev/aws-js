/**
 * @file
 * main.js - AWSjs sample usage on a lambda function
 */

import {RDSInstance} from '../../../src/awsjs';
import {RDSInstanceStatus} from '../../../src/awsjs';
import moment from 'moment';
import {duration} from 'moment';

/**
 * Stop idle DS instances. Idle evaluation is done considering the  value for tag named 'idle timeout'.
 * The tag value should be set using ISO 8601 durations standard, for more information, please check
 * https://en.wikipedia.org/wiki/ISO_8601#Durations
 * In case such tag does not exist or its value is not valid, the instance is not stoped.
 * Please note that only instances with the tag application name set to AWSJS will be evaluated
 * @param {Object} event Event data passed on by AWS.
 */
export async function stopIdleInstances(event) {
  const instances = await RDSInstance.getInstancesByTag('application name', 'AWSJS');

  const instanceCount = instances.length;
  console.log('stopIdleInstances: Found %d instance(s)', instanceCount);
  const operations = [];

  for (const instance of instances) {
    if (instance.Status != RDSInstanceStatus.Available) {
      console.log('stopIdleInstances Instance %s will not be stoped. It is not available, curent status is %s',
          instance.Identifier, instance.Status);

      operations.push({
        InstanceId: instance.Identifier,
        PreviousStatus: instance.Status,
        Operation: 'None (Instance status != ' + RDSInstanceStatus.Available + ')',
      });

      continue;
    }

    const tagValue = await instance.getTagValue('idle timeout');
    if (null == tagValue || tagValue == '') {
      console.log('stopIdleInstances: Instance %s will not be stoped. Idle timeout tag undefined or empty',
          instance.Identifier);

      operations.push({
        InstanceId: instance.Identifier,
        PreviousStatus: instance.Status,
        Operation: 'None (Invalid idle time out tag value)',
      });

      continue;
    }

    const timespan = duration(tagValue);
    if (!timespan.isValid) {
      console.log('stopIdleInstances: Instance %s ignored. Iddle timeout tag not valid', instance.Identifier);

      operations.push({
        InstanceId: instance.Identifier,
        PreviousStatus: instance.Status,
        Operation: 'None (Invalid idle timeout tag value)',
      });

      continue;
    }

    // subtract the timespan to the current time to get our evaluation interval start time
    const endTime = moment(); // now
    const startTime = moment(endTime).subtract(timespan);

    console.log('stopIdleInstances: Performing idle evaluation from %s', startTime.toISOString());
    const isIdle = await instance.getIsIdle(startTime.toDate());
    if (isIdle) {
      console.log('stopIdleInstances: Requesting instance stop [%s]', instance.Identifier);
      instance.stop();

      operations.push({
        InstanceId: instance.Identifier,
        PreviousStatus: instance.Status,
        Operation: 'Stop requested',
      });
    } else {
      console.log('stopIdleInstances: Instance %s will not be stoped. It was evaluated as not idle',
          instance.Identifier);

      operations.push({
        InstanceId: instance.Identifier,
        PreviousStatus: instance.Status,
        Operation: 'None (Instance not idle)',
      });
    }
  }

  return responseUtils.success({
    'instanceCount': instanceCount,
    'operations': operations,
  });
}
