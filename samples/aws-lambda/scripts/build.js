const fs = require('fs');

fs.copyFile('package.json', 'dist/package.json', (err) => {
  if (err) throw err;
});
fs.copyFile('serverless.yml', 'dist/serverless.yml', (err) => {
  if (err) throw err;
});
