/**
 * @file
 * main.js - AWSjs sample usage on RDS instances
 *
 * use node main to run this sample
 * Please note you first need to configure AWS CLI. Please check
 * https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/configuring-the-jssdk.html
 */

const AWSjs = require('../../src/awsjs');

/**
 * Lists and returns RDS instances having the tag 'applicastion name' set to AWSJS
 * @param {Connection} connection The connection instance
 */
listInstances = async (connection) => {
  const instances = await AWSjs.RDSInstance.getAll(connection);
  for (const instance of instances) {
    console.log('Instance %s [Status: %s]', instance.Identifier, instance.Status);
  }
};

// get region, accessKeyId, secretAccessKey

listInstances(new AWSjs.Connection('us-east-1'));
